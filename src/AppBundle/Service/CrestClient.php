<?php


namespace AppBundle\Service;

use GuzzleHttp\Client;



class CrestClient
{


	CONST BASEURL = "https://crest-tq.eveonline.com";


	protected $client;
    
	public function __construct()
	{
		$this->client = new Client([
			'base_uri' => "https://crest-tq.eveonline.com",
			'timeout' => 2.0,
			'verify' => false
		]);
		//$this->client->setDefaultOption('verify', false);

	}


	public function get($url)
	{
		
		$response = $this->client->request('GET', $url, array())->getBody()->getContents();

		return json_decode($response,true);

	}


	// maybe needed not for now
	protected function autherize()
	{
		
	}


    
}
