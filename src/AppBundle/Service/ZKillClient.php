<?php
/**
 * Created by PhpStorm.
 * User: hansolo
 * Date: 7-4-2017
 * Time: 9:35
 */

namespace AppBundle\Service;
use GuzzleHttp\Client;

class ZKillClient
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => "https://zkillboard.com/api",
            'timeout' => 2.0,
            'verify' => false

        ]);
        //$this->client->setDefaultOption('verify', false);

    }


    public function get($url)
    {


        $response = $this->client->request('GET', $url, ['headers'        => ['Accept-Encoding' => 'gzip','User-Agent'=>'Killmailcolector Eningly'],
            'decode_content' => true])->get;

        dump($response);
        die;
        return json_decode($response,true);

    }
}