<?php
/**
 * Created by PhpStorm.
 * User: hansolo
 * Date: 7-4-2017
 * Time: 8:59
 */


namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="regions")
 */

class Region
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $href;

    /**
     * @ORM\Column(type="integer")
     */
    private $eveId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $eveIdString;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @param mixed $href
     */
    public function setHref($href)
    {
        $this->href = $href;
    }

    /**
     * @return mixed
     */
    public function getEveId()
    {
        return $this->eveId;
    }

    /**
     * @param mixed $eveId
     */
    public function setEveId($eveId)
    {
        $this->eveId = $eveId;
    }

    /**
     * @return mixed
     */
    public function getEveIdString()
    {
        return $this->eveIdString;
    }

    /**
     * @param mixed $eveIdString
     */
    public function setEveIdString($eveIdString)
    {
        $this->eveIdString = $eveIdString;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }






}