<?php

/**
 * Created by PhpStorm.
 * User: hansolo
 * Date: 23-3-2017
 * Time: 9:47
 */

// src/AppBundle/Entity/Product.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="type")
 */
class Type
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $href;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $eveId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $eveIdString;

    /**
     * @ORM\ManyToOne(targetEntity="MarketGroup", inversedBy="types")
     * @ORM\JoinColumn(name="groups_id", referencedColumnName="id")
     */
    private $groups;


    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @param mixed $href
     */
    public function setHref($href)
    {
        $this->href = $href;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getEveId()
    {
        return $this->eveId;
    }

    /**
     * @param mixed $eveId
     */
    public function setEveId($eveId)
    {
        $this->eveId = $eveId;
    }

    /**
     * @return mixed
     */
    public function getEveIdString()
    {
        return $this->eveIdString;
    }

    /**
     * @param mixed $eveIdString
     */
    public function setEveIdString($eveIdString)
    {
        $this->eveIdString = $eveIdString;
    }




}