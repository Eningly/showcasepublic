<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\MarketGroup;
use AppBundle\Entity\Region;
use AppBundle\Entity\Type;
use Symfony\Component\HttpFoundation\Response;
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $client = $this->get('app.crest.client');

        $response = $client->get('/market/types/');


        dump($response);
        die;

    }

    /**
     * @Route("/market", name="market")
     */
    public function typeAction(Request $request)
    {
        set_time_limit(2000);

        $counterPages = 1;
        $em = $this->getDoctrine()->getManager();
        $client = $this->get('app.crest.client');

        $response = $client->get('/market/types/');

        $groupRepo = $em->getRepository('AppBundle:MarketGroup');





        $groups = [];
        while ($counterPages <= $response['pageCount']){
            $counterInsert = 0;

            foreach ($response['items'] as $item){

                $type = new Type();
                $type->setName($item['type']['name']);

                $type->setHref($item['type']['href']);
                $type->setEveId($item['type']['id']);
                $type->setEveIdString($item['type']['id_str']);

                if (array_key_exists($item['marketGroup']['id_str'],$groups)){
                    $type->setGroups($groups[$item['marketGroup']['id_str']]);
                }else{

                    $group = $groupRepo->findOneBy(['eveIdString'=>$item['marketGroup']['id_str']]);

                    $groups[$item['marketGroup']['id_str']] = $group;

                    $type->setGroups($group);
                }

                $em->persist($type);

                if ($counterInsert == 15){
                    $em->flush();
                    $counterInsert = 0;
                }

                $counterInsert++;

            }
            $em->flush();
            $counterPages++;
            $response = $client->get('/market/types/?page='.$counterPages);

        }



        return new Response(
            '<html><body>done</body></html>'
        );
        
    }


    /**
     * @Route("/group", name="group")
     */
    public function groupAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $client = $this->get('app.crest.client');

        $response = $client->get('/market/groups/');

        $counter = 0;
        foreach ($response["items"] as $group){

            $newGroup = new MarketGroup();
            $newGroup->setName($group['name']);
            $newGroup->setDescription($group['description']);
            $newGroup->setHref($group['href']);
            $newGroup->setEveId($group['id']);
            $newGroup->setEveIdString($group['id_str']);
            $em->persist($newGroup);

            if ($counter == 15){
                $em->flush();
                $counter = 0;
            }

            $counter++;

        }
        $em->flush();
        return new Response(
            '<html><body>done</body></html>'
        );

       // $this->render('lucky/number.html.twig', array('name' => $name));


    }
    /**
     * @Route("/clean", name="type")
     */
    public function cleanAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $repo = $em->getRepository('AppBundle:MarketGroup');

        $result = $repo->findAll();

        $counter = 0;

        foreach ($result as $group){

            $em->remove($group);

            if ($counter == 15){
                $em->flush();
                $counter = 0;
            }
            $counter++;
        }
        $em->flush();
        return new Response(
            '<html><body>done</body></html>'
        );
    }

    /**
     * @Route("/region", name="region")
     */
    public function regionAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $client = $this->get('app.crest.client');

        $response = $client->get('/regions/');

        $counter = 0;
        foreach ($response['items'] as $item){
            $region = new Region();

            $region->setName($item['name']);
            $region->setHref($item['href']);
            $region->setEveId($item['id']);
            $region->setEveIdString($item['id_str']);
            $em->persist($region);
            if ($counter == 15){
                $em->flush();
                $counter = 0;
            }

            $counter++;


        }
        $em->flush();
        return new Response(
            '<html><body>done</body></html>'
        );


    }


    /**
     * @Route("/killmail/{regionId}", name="killmail")
     */
    public function killmailAction(Request $request,$regionId){

        $zclient = $this->get('app.zkil.client');

        dump('/kills/regionID/'.$regionId);
        $response = $zclient->get('/kills/regionID/'.$regionId.'/');

        dump($response);

        die;

    }

//    /**
//     * @Route("/group", name"group")
//     */
//    public function groupAction(Request $request)
//    {
//
//        $client = $this->get('app.crest.client');
//
//
//        dump($response);
//        die;
//
//    }
}
